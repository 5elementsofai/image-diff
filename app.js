const looksSame = require('looks-same');
const express = require('express');
const fileUpload = require('express-fileupload');
const cors = require('cors');
const bodyParser = require('body-parser');
const logger = require('morgan');

const app = express();
app.set('port', process.env.PORT || 3000);

app.use(fileUpload({
    createParentPath: true
}));
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(logger('dev'));

app.get('/ping', (req, res) => {
    res.send('pong');
});

app.get('/info', (req, res) => {
    res.json({
        'version': '1.0',
        'project': '5 elements of AI',
        'service': 'image-diff',
        'language': 'node',
        'type': 'api',
    });
})

app.post('/diff', (req, res) => {
    console.log('req ', req);
    if (!('reference' in req.files && 'current' in req.files)) {
        return res.send('You have to upload reference and current');
    }

    const reference = req.files.reference.data;
    const current = req.files.current.data;

    if (req.accepts('png')) {
        looksSame.createDiff({
            reference,
            current,
            highlightColor: '#ff0000',
            tolerance: 0,
        }, (error, img) => {
            if (error) {
                res.send(error);
            } else {
                console.log('args ', arguments)

                res.set('Content-Type', 'image/png');
                res.writeHead(200, {
                    'Content-Type': 'image/png',
                    'Content-Length': img.length
                });
                res.end(img);
            }
        });
    }
    else if (req.accepts('json')) {
        const options = {
            shouldCluster: true, 
            clustersSize: 10
        };
        looksSame(reference, current, options, (error, diff) => {
            if (error) {
                res.send(error.message);
            } else {
                res.json(diff);
            }
        });
    } 
    else {
        res.send('You have to set an accept header (image/png or application/json)')
    }
});

app.listen(app.get('port'), function () {
    console.log('Express server listening on port ' + app.get('port'));
});

